<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetPageProperty('title', 'Отзыв - '.$arResult['NAME']);
$APPLICATION->SetPageProperty('h1', 'Отзыв - '.$arResult['NAME'].' - '.$arResult['PROPERTIES']['COMPANY']['VALUE']);
$APPLICATION->SetPageProperty('keywords', 'лучшие, отзывы, '.$arResult['PROPERTIES']['COMPANY']['VALUE']);
$APPLICATION->SetPageProperty('description', $arResult["PREVIEW_TEXT"]);
?>
<div class="review-block">
	<div class="review-text">
		<div class="review-text-cont">
			<?if($arResult["DETAIL_TEXT"] <> ''):?>
				<?echo $arResult["DETAIL_TEXT"];?>
			<?else:?>
				<?echo $arResult["PREVIEW_TEXT"];?>
			<?endif?>
		</div>
		<div class="review-autor">
		<?= $arResult["NAME"].', '.$arResult["DISPLAY_ACTIVE_FROM"].', '.$arResult['PROPERTIES']['POSITION']['VALUE'].', '.$arResult['PROPERTIES']['COMPANY']['VALUE']?>
		</div>
	</div>
	<div style="clear: both;" class="review-img-wrap">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
		<?else:?>
			<img
			class="detail_picture"
			border="0"
			src="<?=SITE_TEMPLATE_PATH?>/img/rew/no_photo.jpg"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>	
	<?endif?></div>
</div>
<?if (count($arResult["FIELDS"]) != 0):?>
<div class="exam-review-doc">
<p>Документы:</p>
	<?$i = 0?>
	<?foreach($arResult["FIELDS"] as $code=>$value):
		$i++;
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><div class="exam-review-item-doc"><img class="rew-doc-ico" src="<?=$value["SRC"]?>"><a href="<?=$value["SRC"]?>">Файл <?=$i?></a></div><?
			}
		}
		?>
	<?endforeach;?>
</div>
<?endif?>