<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="item-wrap">
	<div class="rew-footer-carousel">
		<? foreach ($arResult["ITEMS"] as $arItem) : ?>
			<div class="item">
				<div class="side-block side-opin">
					<div class="inner-block">
						<div class="title">
							<div class="photo-block">
								<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])) : ?>
									<? $arItem["PREVIEW_PICTURE"] = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width' => 39, 'height' => 39), BX_RESIZE_IMAGE_EXACT);
									?>
									<img class="preview_picture" border="0" src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>" width="<?= $arItem["PREVIEW_PICTURE"]["WIDTH"] ?>" height="<?= $arItem["PREVIEW_PICTURE"]["HEIGHT"] ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>" title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>" style="float:left" />
								<? else : ?>
									<img class="preview_picture" border="0" src="<?= SITE_TEMPLATE_PATH ?>/img/rew/no_photo.jpg" width="39" height="39" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>" title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>" style="float:left" />
								<? endif ?>
							</div>
							<div class="name-block"><a href=""><?= $arItem["NAME"] ?></a></div>
							<div class="pos-block"><?= $arItem["DISPLAY_PROPERTIES"]['POSITION']['DISPLAY_VALUE'] . ',"' . $arItem["DISPLAY_PROPERTIES"]['COMPANY']['DISPLAY_VALUE'] . '"' ?></div>
						</div>
						<div class="text-block">“<? if (strlen($arItem["PREVIEW_TEXT"]) > 150) echo substr($arItem["PREVIEW_TEXT"], 0, 147) . '...';
													else echo $arItem["PREVIEW_TEXT"] . '"'; ?></div>
					</div>
				</div>
			</div>
		<? endforeach; ?>
	</div>
</div>