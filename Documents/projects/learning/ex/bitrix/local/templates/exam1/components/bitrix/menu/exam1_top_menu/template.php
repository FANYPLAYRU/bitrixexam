<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<nav class="nav">
	<div class="inner-wrap">
		<div class="menu-block popup-wrap">
			<a href="" class="btn-menu btn-toggle"></a>
			<div class="menu popup-block">
				<ul>

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li <? if ($arItem["LINK"] == "/") echo 'class="main-page"'; ?>><a  href="<?=$arItem["LINK"]?>" <? if (is_set($arItem['PARAMS']['color']) && $arItem['PARAMS']['color'] != "") echo 'class="color-'.$arItem['PARAMS']['color'].'"';?>><?=$arItem["TEXT"]?></a>
				<ul>
				<? if (is_set($arItem['PARAMS']['btn_desc']) && $arItem['PARAMS']['btn_desc'] != "") echo '<div class="menu-text">'.$arItem['PARAMS']['btn_desc'].'</div>';?>
		<?else:?>
			<li <? if ($arItem["LINK"] == "/") echo 'class="main-page"'; ?>><a href="<?=$arItem["LINK"]?>" <? if (is_set($arItem['PARAMS']['color']) && $arItem['PARAMS']['color'] != "") echo 'class="color-'.$arItem['PARAMS']['color'].'"';?> class="parent"><?=$arItem["TEXT"]?></a>
				<ul>
				<? if (is_set($arItem['PARAMS']['btn_desc']) && $arItem['PARAMS']['btn_desc'] != "") echo '<div class="menu-text">'.$arItem['PARAMS']['btn_desc'].'</div>';?>
		<?endif?>

	<?else:?>
			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li <? if ($arItem["LINK"] == "/") echo 'class="main-page"'; ?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li <? if ($arItem["LINK"] == "/") echo 'class="main-page"'; ?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>
	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
				</ul>
			</div>
		</div>
	</div>
</nav>
<?endif?>